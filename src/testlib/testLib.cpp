#include <stdio.h>
#include <stdlib.h>
#include <testLib.h>

// 領域外アクセステスト用
void test11(void)
{
	int *p;

	p = (int *) malloc(sizeof(int)*4);
	p[4] = 0; // NG
	free(p);

}

// 解放漏れ
void test12(void)
{
	int *p;

	p = (int *)malloc(sizeof(int) * 4);

	// pをfreeしていない
}

// 2重開放
void test13(void)
{
	int *p;

	p = (int *)malloc(sizeof(int) * 4);

	free(p);
	free(p); // pはfree済み
	
}

// freeした領域へのアクセス
void test14(void)
{
	int *p;

	p = (int *)malloc(sizeof(int) * 4);

	free(p);
	p[0] = 0; // free済み領域への書き込み
}

//初期化していない領域の参照
void test15(void)
{
	int x;
	printf("%d\n", x); // xは未初期化
}
