#include <stdio.h>
#include <stdlib.h>

// 領域外アクセステスト用
int main(void)
{
	int *p;

	p = (int *) malloc(sizeof(int)*4);
	p[4] = 0; // NG
	free(p);

	return 0;
}