#include <stdio.h>
#include <stdlib.h>
// freeした領域へのアクセス
int main(void)
{
	int *p;

	p = (int *)malloc(sizeof(int) * 4);

	free(p);
	p[0] = 0; // free済み領域への書き込み
}