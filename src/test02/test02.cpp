#include <stdio.h>
#include <stdlib.h>

int func(void);

// 解放漏れテスト
int main(void)
{
	func();

	return 0;
}

int func(void)
{
	int *p;

	p = (int *)malloc(sizeof(int) * 4);

	// pをfreeしていない
	return 0;
}
