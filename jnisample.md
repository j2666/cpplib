# valgrindで、jni使って呼び出しているライブラリに対して検出できるか試してみた


## 機能一覧
|No|チェック項目|可否|
|--|--|--|
|01|領域外書き込み|〇|
|02|解放漏れ|〇|
|03|2重開放|〇|
|04|解放済み領域への書き込み|〇|
|05|初期化していない領域の参照|〇|

## 領域外アクセスの検出例(test11)
## 解放漏れの検出例(test12)

### 抑止なしで実行した場合

`valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./no_suppress.log /usr/lib/jvm/java-11-openjdk-amd64/bin/java @/tmp/cp_8obejomucoulskm2r2v2bg4r7.argfile com.example.App `

jvm関係のものが大量に出て見づらいですがtestLibのエラーを検出。
マークダウンに埋め込むと重くなるので別ファイルに切り出してます。

- [実行結果(no_suppress.log)](./no_suppress.log)

### 抑止設定を入れて実行した場合

-g オプション付きで実行したログから
`cat ログファイル | grep -v "^–" | grep -v "^==" > 抑止ファイル`
を出力し、今回のテストコードを検出した下記の部分を削除

```
{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: definite
   fun:malloc
   fun:test12
   obj:*
   obj:*
   obj:*
   obj:/usr/lib/jvm/java-11-openjdk-amd64/lib/server/libjvm.so
   obj:/usr/lib/jvm/java-11-openjdk-amd64/lib/server/libjvm.so
   obj:/usr/lib/jvm/java-11-openjdk-amd64/lib/server/libjvm.so
   obj:/usr/lib/jvm/java-11-openjdk-amd64/lib/jli/libjli.so
   obj:/usr/lib/jvm/java-11-openjdk-amd64/lib/jli/libjli.so
   fun:start_thread
   fun:clone
}
```


`valgrind --suppressions=suppression_00 --leak-check=full --show-reachable=yes --error-limit=no --log-file=./with_suppress.log /usr/lib/jvm/java-11-openjdk-amd64/bin/java @/tmp/cp_8obejomucoulskm2r2v2bg4r7.argfile com.example.App`



- [抑止定義(suppression_00)](./suppression_00)



- [実行結果(with_suppress.log)](./with_suppress.log)


## 2重開放の検出例(test13)
## 解放済み領域への書き込み(test14)
## 初期化していない領域の参照(test15)
