# ツールのリスト

|ツール名|コンパイル要否|ソースコード修正|参考サイト|公式|
|--|--|--|--|--|
|AddressSanitizer|必要|不要|https://kivantium.hateblo.jp/entry/2018/07/14/233027|https://github.com/google/sanitizers/wiki/AddressSanitizer|
|valgrind|不要|不要|http://linuxc.info/debug/valgrind.html|https://access.redhat.com/documentation/ja-jp/red_hat_enterprise_linux/7/html-single/developer_guide/index#monitoring-performance \n https://valgrind.org/docs/manual/manual.html|
|mtrace|必要|必要|https://kivantium.hateblo.jp/entry/2018/07/14/233027|?|
|Dr. Memory|不要|不要|https://mjt.hatenadiary.com/entry/20120815/p3|https://drmemory.org/|
|AddressSanitizer|必要|不要|https://goyoki.hatenablog.com/entry/2016/01/27/024327 \n https://zenn.dev/mu/articles/f0b6f07bf6b397 | https://github.com/google/sanitizers/wiki/AddressSanitizerFlags#run-time-flags|
|memleax|不要|不要|https://blog.yukirii.dev/using-memleax-to-find-memory-leaks/|-|


## テスト用ライブラリのビルド&導入方法

落としてきたところで以下を実行
```shell-session{.line-numbers}
$ ./sh/build.sh
g++ -I../../include   -shared -fPIC  -o ../../lib/libtestLib.so testLib.cpp
g++ -I../../include   -shared -fPIC  -o ../../lib/libheapTestLib.so heapTestLib.cpp
./testlib
make: '../../lib/libtestLib.so' is up to date.
./test14
g++  -I../../include -c test14.cpp
g++ -o ../../bin/test14 test14.o -L../../lib -ltestLib
./test11
g++  -I../../include -c test11.cpp
g++ -o ../../bin/test11 test11.o -L../../lib -ltestLib
./test15
g++  -I../../include -c test15.cpp
g++ -o ../../bin/test15 test15.o -L../../lib -ltestLib
./test13
g++  -I../../include -c test13.cpp
g++ -o ../../bin/test13 test13.o -L../../lib -ltestLib
./test03
g++  -I../include -c test03.cpp
g++ -o ../../bin/test03 test03.o -L../../lib -ltestLib
./test04
g++  -I../include -c test04.cpp
g++ -o ../../bin/test04 test04.o -L../../lib -ltestLib
./test02
g++  -I../include -c test02.cpp
g++ -o ../../bin/test02 test02.o -L../../lib -ltestLib
./test01
g++  -I../include -c test01.cpp
g++ -o ../../bin/test01 test01.o -L../../lib -ltestLib
./test12
g++  -I../../include -c test12.cpp
g++ -o ../../bin/test12 test12.o -L../../lib -ltestLib
./test05
g++  -I../include -c test05.cpp
g++ -o ../../bin/test05 test05.o -L../../lib -ltestLib
./heapTestlib
make: '../../lib/libheapTestLib.so' is up to date.
./test00
g++  -I../include -c test00.cpp
g++ -o ../../bin/test00 test00.o -L../../lib -ltestLib
```

生成したライブラリを読み込めるようにするために、/usr/local/lib配下にリンクを張る
```shell-session{.line-numbers}
$ sudo ln -s /home/kogakz/cpplib/lib/lib* /usr/local/lib/
```
## valgrind
参考にしたサイト
http://linuxc.info/debug/valgrind.html


### インストール方法
sudo apt install valgrind

### 機能一覧
|No|チェック項目|可否|
|--|--|--|
|01|領域外書き込み|〇|
|02|解放漏れ|〇|
|03|2重開放|〇|
|04|解放済み領域への書き込み|〇|
|05|初期化していない領域の参照|〇|

### 領域外アクセスの検出例(test01,test11)
以下の2パターンのサンプルを出す
1. 静的リンクの例
2. ダイナミックリンク（共有ライブラリ）での例
    - gあり、なし


####  静的リンクの例

オブジェクトを直接リンクしているケースでの例

`-g`ありの場合
**Invalid write** として、**test01.cpp:10**で行われていると検出されている
```
==3837== Memcheck, a memory error detector
==3837== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==3837== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==3837== Command: ./target/test01
==3837== Parent PID: 3836
==3837== 
==3837== Invalid write of size 4
==3837==    at 0x10918B: main (test01.cpp:10)
==3837==  Address 0x4a4e050 is 0 bytes after a block of size 16 alloc'd
==3837==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==3837==    by 0x10917E: main (test01.cpp:9)
==3837== 
==3837== 
==3837== HEAP SUMMARY:
==3837==     in use at exit: 0 bytes in 0 blocks
==3837==   total heap usage: 1 allocs, 1 frees, 16 bytes allocated
==3837== 
==3837== All heap blocks were freed -- no leaks are possible
==3837== 
==3837== For lists of detected and suppressed errors, rerun with: -s
==3837== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```


#### ダイナミックリンク（共有ライブラリ）での例

`-g`オプションでビルドした場合は、以下のようにソースまででる。
**Invalid write** として、**testLib.cpp:10**で行われていると検出されている


```
==16492== Memcheck, a memory error detector
==16492== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==16492== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==16492== Command: ./bin/test11
==16492== Parent PID: 16466
==16492== 
==16492== Invalid write of size 4
==16492==    at 0x484B17B: test11() (testLib.cpp:10)
==16492==    by 0x109155: main (in /home/kogakz/cpplib/bin/test11)
==16492==  Address 0x4a53050 is 0 bytes after a block of size 16 alloc'd
==16492==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==16492==    by 0x484B16E: test11() (testLib.cpp:9)
==16492==    by 0x109155: main (in /home/kogakz/cpplib/bin/test11)
==16492== 
==16492== 
==16492== HEAP SUMMARY:
==16492==     in use at exit: 0 bytes in 0 blocks
==16492==   total heap usage: 1 allocs, 1 frees, 16 bytes allocated
==16492== 
==16492== All heap blocks were freed -- no leaks are possible
==16492== 
==16492== For lists of detected and suppressed errors, rerun with: -s
==16492== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```

**`-g`オプションなし**でビルドした場合は、以下のようにライブラリ名まででる。


```
==15942== Memcheck, a memory error detector
==15942== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==15942== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==15942== Command: ./bin/test11
==15942== Parent PID: 15919
==15942== 
==15942== Invalid write of size 4
==15942==    at 0x484B17B: test11() (in /home/kogakz/cpplib/lib/libtestLib.so)
==15942==    by 0x109155: main (in /home/kogakz/cpplib/bin/test11)
==15942==  Address 0x4a53050 is 0 bytes after a block of size 16 alloc'd
==15942==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==15942==    by 0x484B16E: test11() (in /home/kogakz/cpplib/lib/libtestLib.so)
==15942==    by 0x109155: main (in /home/kogakz/cpplib/bin/test11)
==15942== 
==15942== 
==15942== HEAP SUMMARY:
==15942==     in use at exit: 0 bytes in 0 blocks
==15942==   total heap usage: 1 allocs, 1 frees, 16 bytes allocated
==15942== 
==15942== All heap blocks were freed -- no leaks are possible
==15942== 
==15942== For lists of detected and suppressed errors, rerun with: -s
==15942== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```

```
==16492== Memcheck, a memory error detector
==16492== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==16492== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==16492== Command: ./bin/test11
==16492== Parent PID: 16466
==16492== 
==16492== Invalid write of size 4
==16492==    at 0x484B17B: test11() (testLib.cpp:10)
==16492==    by 0x109155: main (in /home/kogakz/cpplib/bin/test11)
==16492==  Address 0x4a53050 is 0 bytes after a block of size 16 alloc'd
==16492==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==16492==    by 0x484B16E: test11() (testLib.cpp:9)
==16492==    by 0x109155: main (in /home/kogakz/cpplib/bin/test11)
==16492== 
==16492== 
==16492== HEAP SUMMARY:
==16492==     in use at exit: 0 bytes in 0 blocks
==16492==   total heap usage: 1 allocs, 1 frees, 16 bytes allocated
==16492== 
==16492== All heap blocks were freed -- no leaks are possible
==16492== 
==16492== For lists of detected and suppressed errors, rerun with: -s
==16492== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```

### 解放漏れの検出例(test02,test12)
以下の2パターンのサンプルを出す
1. 静的リンクの例
2. ダイナミックリンク（共有ライブラリ）での例
    - gあり、なし

####  静的リンクの例
**HEAP SUMMARY** で、**func() (test02.cpp:18)** の解放漏れが検出されている

```
==3838== Memcheck, a memory error detector
==3838== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==3838== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==3838== Command: ./target/test02
==3838== Parent PID: 3836
==3838== 
==3838== 
==3838== HEAP SUMMARY:
==3838==     in use at exit: 16 bytes in 1 blocks
==3838==   total heap usage: 1 allocs, 0 frees, 16 bytes allocated
==3838== 
==3838== 16 bytes in 1 blocks are definitely lost in loss record 1 of 1
==3838==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==3838==    by 0x109172: func() (test02.cpp:18)
==3838==    by 0x109155: main (test02.cpp:9)
==3838== 
==3838== LEAK SUMMARY:
==3838==    definitely lost: 16 bytes in 1 blocks
==3838==    indirectly lost: 0 bytes in 0 blocks
==3838==      possibly lost: 0 bytes in 0 blocks
==3838==    still reachable: 0 bytes in 0 blocks
==3838==         suppressed: 0 bytes in 0 blocks
==3838== 
==3838== For lists of detected and suppressed errors, rerun with: -s
==3838== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)

```

#### ダイナミックリンク（共有ライブラリ）での例

`-g`オプションでビルドした場合は、以下のようにソースまででる。

```
==16493== Memcheck, a memory error detector
==16493== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==16493== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==16493== Command: ./bin/test12
==16493== Parent PID: 16466
==16493== 
==16493== 
==16493== HEAP SUMMARY:
==16493==     in use at exit: 16 bytes in 1 blocks
==16493==   total heap usage: 1 allocs, 0 frees, 16 bytes allocated
==16493== 
==16493== 16 bytes in 1 blocks are definitely lost in loss record 1 of 1
==16493==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==16493==    by 0x484B1A5: test12() (testLib.cpp:20)
==16493==    by 0x109155: main (test12.cpp:7)
==16493== 
==16493== LEAK SUMMARY:
==16493==    definitely lost: 16 bytes in 1 blocks
==16493==    indirectly lost: 0 bytes in 0 blocks
==16493==      possibly lost: 0 bytes in 0 blocks
==16493==    still reachable: 0 bytes in 0 blocks
==16493==         suppressed: 0 bytes in 0 blocks
==16493== 
==16493== For lists of detected and suppressed errors, rerun with: -s
==16493== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)

```


**`-g`オプションなし**でビルドした場合は、以下のようにライブラリ名まででる。

```
==17979== Memcheck, a memory error detector
==17979== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==17979== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==17979== Command: ./bin/test12
==17979== Parent PID: 17944
==17979== 
==17979== 
==17979== HEAP SUMMARY:
==17979==     in use at exit: 16 bytes in 1 blocks
==17979==   total heap usage: 1 allocs, 0 frees, 16 bytes allocated
==17979== 
==17979== 16 bytes in 1 blocks are definitely lost in loss record 1 of 1
==17979==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==17979==    by 0x484B1A5: test12() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17979==    by 0x109155: main (in /home/kogakz/cpplib/bin/test12)
==17979== 
==17979== LEAK SUMMARY:
==17979==    definitely lost: 16 bytes in 1 blocks
==17979==    indirectly lost: 0 bytes in 0 blocks
==17979==      possibly lost: 0 bytes in 0 blocks
==17979==    still reachable: 0 bytes in 0 blocks
==17979==         suppressed: 0 bytes in 0 blocks
==17979== 
==17979== For lists of detected and suppressed errors, rerun with: -s
==17979== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)

```


### 2重開放の検出例(test03,test13)
以下の2パターンのサンプルを出す
1. 静的リンクの例
2. ダイナミックリンク（共有ライブラリ）での例
    - gあり、なし

####  静的リンクの例

**Invalid free() / delete / delete[] / realloc()** として検出されている。

**main (test03.cpp:12)** が、**main (test03.cpp:11)** で解放済みのポインタを開放している


```
==3850== Memcheck, a memory error detector
==3850== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==3850== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==3850== Command: ./target/test03
==3850== Parent PID: 3836
==3850== 
==3850== Invalid free() / delete / delete[] / realloc()
==3850==    at 0x483CA3F: free (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==3850==    by 0x10919A: main (test03.cpp:12)
==3850==  Address 0x4a4e040 is 0 bytes inside a block of size 16 free'd
==3850==    at 0x483CA3F: free (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==3850==    by 0x10918E: main (test03.cpp:11)
==3850==  Block was alloc'd at
==3850==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==3850==    by 0x10917E: main (test03.cpp:9)
==3850== 
==3850== 
==3850== HEAP SUMMARY:
==3850==     in use at exit: 0 bytes in 0 blocks
==3850==   total heap usage: 1 allocs, 2 frees, 16 bytes allocated
==3850== 
==3850== All heap blocks were freed -- no leaks are possible
==3850== 
==3850== For lists of detected and suppressed errors, rerun with: -s
==3850== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```

#### ダイナミックリンク（共有ライブラリ）での例

`-g`オプションでビルドした場合は、以下のようにソースまででる。


```
==16505== Memcheck, a memory error detector
==16505== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==16505== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==16505== Command: ./bin/test13
==16505== Parent PID: 16466
==16505== 
==16505== Invalid free() / delete / delete[] / realloc()
==16505==    at 0x483CA3F: free (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==16505==    by 0x484B1DE: test13() (testLib.cpp:33)
==16505==    by 0x109155: main (test13.cpp:7)
==16505==  Address 0x4a53040 is 0 bytes inside a block of size 16 free'd
==16505==    at 0x483CA3F: free (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==16505==    by 0x484B1D2: test13() (testLib.cpp:32)
==16505==    by 0x109155: main (test13.cpp:7)
==16505==  Block was alloc'd at
==16505==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==16505==    by 0x484B1C2: test13() (testLib.cpp:30)
==16505==    by 0x109155: main (test13.cpp:7)
==16505== 
==16505== 
==16505== HEAP SUMMARY:
==16505==     in use at exit: 0 bytes in 0 blocks
==16505==   total heap usage: 1 allocs, 2 frees, 16 bytes allocated
==16505== 
==16505== All heap blocks were freed -- no leaks are possible
==16505== 
==16505== For lists of detected and suppressed errors, rerun with: -s
==16505== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```


**`-g`オプションなし**でビルドした場合は、以下のようにライブラリ名まででる。

```
==17980== Memcheck, a memory error detector
==17980== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==17980== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==17980== Command: ./bin/test13
==17980== Parent PID: 17944
==17980== 
==17980== Invalid free() / delete / delete[] / realloc()
==17980==    at 0x483CA3F: free (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==17980==    by 0x484B1DE: test13() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17980==    by 0x109155: main (in /home/kogakz/cpplib/bin/test13)
==17980==  Address 0x4a53040 is 0 bytes inside a block of size 16 free'd
==17980==    at 0x483CA3F: free (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==17980==    by 0x484B1D2: test13() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17980==    by 0x109155: main (in /home/kogakz/cpplib/bin/test13)
==17980==  Block was alloc'd at
==17980==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==17980==    by 0x484B1C2: test13() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17980==    by 0x109155: main (in /home/kogakz/cpplib/bin/test13)
==17980== 
==17980== 
==17980== HEAP SUMMARY:
==17980==     in use at exit: 0 bytes in 0 blocks
==17980==   total heap usage: 1 allocs, 2 frees, 16 bytes allocated
==17980== 
==17980== All heap blocks were freed -- no leaks are possible
==17980== 
==17980== For lists of detected and suppressed errors, rerun with: -s
==17980== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)

```

### 解放済み領域への書き込み(test04,test14)
以下の2パターンのサンプルを出す
1. 静的リンクの例
2. ダイナミックリンク（共有ライブラリ）での例
    - gあり、なし

####  静的リンクの例

**Invalid write**として**test04.cpp:11**が検出されている


```
==3857== Memcheck, a memory error detector
==3857== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==3857== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==3857== Command: ./target/test04
==3857== Parent PID: 3836
==3857== 
==3857== Invalid write of size 4
==3857==    at 0x109193: main (test04.cpp:11)
==3857==  Address 0x4a4e040 is 0 bytes inside a block of size 16 free'd
==3857==    at 0x483CA3F: free (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==3857==    by 0x10918E: main (test04.cpp:10)
==3857==  Block was alloc'd at
==3857==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==3857==    by 0x10917E: main (test04.cpp:8)
==3857== 
==3857== 
==3857== HEAP SUMMARY:
==3857==     in use at exit: 0 bytes in 0 blocks
==3857==   total heap usage: 1 allocs, 1 frees, 16 bytes allocated
==3857== 
==3857== All heap blocks were freed -- no leaks are possible
==3857== 
==3857== For lists of detected and suppressed errors, rerun with: -s
==3857== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)

```
#### ダイナミックリンク（共有ライブラリ）での例

`-g`オプションでビルドした場合は、以下のようにソースまででる。

```
==16512== Memcheck, a memory error detector
==16512== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==16512== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==16512== Command: ./bin/test14
==16512== Parent PID: 16466
==16512== 
==16512== Invalid write of size 4
==16512==    at 0x484B20C: test14() (testLib.cpp:45)
==16512==    by 0x109155: main (test14.cpp:7)
==16512==  Address 0x4a53040 is 0 bytes inside a block of size 16 free'd
==16512==    at 0x483CA3F: free (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==16512==    by 0x484B207: test14() (testLib.cpp:44)
==16512==    by 0x109155: main (test14.cpp:7)
==16512==  Block was alloc'd at
==16512==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==16512==    by 0x484B1F7: test14() (testLib.cpp:42)
==16512==    by 0x109155: main (test14.cpp:7)
==16512== 
==16512== 
==16512== HEAP SUMMARY:
==16512==     in use at exit: 0 bytes in 0 blocks
==16512==   total heap usage: 1 allocs, 1 frees, 16 bytes allocated
==16512== 
==16512== All heap blocks were freed -- no leaks are possible
==16512== 
==16512== For lists of detected and suppressed errors, rerun with: -s
==16512== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)

```


**`-g`オプションなし**でビルドした場合は、以下のようにライブラリ名まででる。

```
==17987== Memcheck, a memory error detector
==17987== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==17987== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==17987== Command: ./bin/test14
==17987== Parent PID: 17944
==17987== 
==17987== Invalid write of size 4
==17987==    at 0x484B20C: test14() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17987==    by 0x109155: main (in /home/kogakz/cpplib/bin/test14)
==17987==  Address 0x4a53040 is 0 bytes inside a block of size 16 free'd
==17987==    at 0x483CA3F: free (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==17987==    by 0x484B207: test14() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17987==    by 0x109155: main (in /home/kogakz/cpplib/bin/test14)
==17987==  Block was alloc'd at
==17987==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==17987==    by 0x484B1F7: test14() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17987==    by 0x109155: main (in /home/kogakz/cpplib/bin/test14)
==17987== 
==17987== 
==17987== HEAP SUMMARY:
==17987==     in use at exit: 0 bytes in 0 blocks
==17987==   total heap usage: 1 allocs, 1 frees, 16 bytes allocated
==17987== 
==17987== All heap blocks were freed -- no leaks are possible
==17987== 
==17987== For lists of detected and suppressed errors, rerun with: -s
==17987== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)

```

### 初期化していない領域の参照(test05,test15)

以下の2パターンのサンプルを出す
1. 静的リンクの例
2. ダイナミックリンク（共有ライブラリ）での例
    - gあり、なし

####  静的リンクの例
**Use of uninitialised value**として**test05.cpp:8**を検出している


```
==3869== Memcheck, a memory error detector
==3869== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==3869== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==3869== Command: ./target/test05
==3869== Parent PID: 3836
==3869== 
==3869== Conditional jump or move depends on uninitialised value(s)
==3869==    at 0x48D0988: __vfprintf_internal (vfprintf-internal.c:1687)
==3869==    by 0x48BAD6E: printf (printf.c:33)
==3869==    by 0x10916A: main (test05.cpp:8)
==3869== 
==3869== Use of uninitialised value of size 8
==3869==    at 0x48B46CB: _itoa_word (_itoa.c:179)
==3869==    by 0x48D05A4: __vfprintf_internal (vfprintf-internal.c:1687)
==3869==    by 0x48BAD6E: printf (printf.c:33)
==3869==    by 0x10916A: main (test05.cpp:8)
==3869== 
==3869== Conditional jump or move depends on uninitialised value(s)
==3869==    at 0x48B46DD: _itoa_word (_itoa.c:179)
==3869==    by 0x48D05A4: __vfprintf_internal (vfprintf-internal.c:1687)
==3869==    by 0x48BAD6E: printf (printf.c:33)
==3869==    by 0x10916A: main (test05.cpp:8)
==3869== 
==3869== Conditional jump or move depends on uninitialised value(s)
==3869==    at 0x48D1258: __vfprintf_internal (vfprintf-internal.c:1687)
==3869==    by 0x48BAD6E: printf (printf.c:33)
==3869==    by 0x10916A: main (test05.cpp:8)
==3869== 
==3869== Conditional jump or move depends on uninitialised value(s)
==3869==    at 0x48D071E: __vfprintf_internal (vfprintf-internal.c:1687)
==3869==    by 0x48BAD6E: printf (printf.c:33)
==3869==    by 0x10916A: main (test05.cpp:8)
==3869== 
==3869== 
==3869== HEAP SUMMARY:
==3869==     in use at exit: 0 bytes in 0 blocks
==3869==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==3869== 
==3869== All heap blocks were freed -- no leaks are possible
==3869== 
==3869== Use --track-origins=yes to see where uninitialised values come from
==3869== For lists of detected and suppressed errors, rerun with: -s
==3869== ERROR SUMMARY: 5 errors from 5 contexts (suppressed: 0 from 0)
```

#### ダイナミックリンク（共有ライブラリ）での例

`-g`オプションでビルドした場合は、以下のようにソースまででる。

```
==16513== Memcheck, a memory error detector
==16513== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==16513== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==16513== Command: ./bin/test15
==16513== Parent PID: 16466
==16513== 
==16513== Conditional jump or move depends on uninitialised value(s)
==16513==    at 0x48D5988: __vfprintf_internal (vfprintf-internal.c:1687)
==16513==    by 0x48BFD6E: printf (printf.c:33)
==16513==    by 0x484B236: test15() (testLib.cpp:52)
==16513==    by 0x109155: main (test15.cpp:7)
==16513== 
==16513== Use of uninitialised value of size 8
==16513==    at 0x48B96CB: _itoa_word (_itoa.c:179)
==16513==    by 0x48D55A4: __vfprintf_internal (vfprintf-internal.c:1687)
==16513==    by 0x48BFD6E: printf (printf.c:33)
==16513==    by 0x484B236: test15() (testLib.cpp:52)
==16513==    by 0x109155: main (test15.cpp:7)
==16513== 
==16513== Conditional jump or move depends on uninitialised value(s)
==16513==    at 0x48B96DD: _itoa_word (_itoa.c:179)
==16513==    by 0x48D55A4: __vfprintf_internal (vfprintf-internal.c:1687)
==16513==    by 0x48BFD6E: printf (printf.c:33)
==16513==    by 0x484B236: test15() (testLib.cpp:52)
==16513==    by 0x109155: main (test15.cpp:7)
==16513== 
==16513== Conditional jump or move depends on uninitialised value(s)
==16513==    at 0x48D6258: __vfprintf_internal (vfprintf-internal.c:1687)
==16513==    by 0x48BFD6E: printf (printf.c:33)
==16513==    by 0x484B236: test15() (testLib.cpp:52)
==16513==    by 0x109155: main (test15.cpp:7)
==16513== 
==16513== Conditional jump or move depends on uninitialised value(s)
==16513==    at 0x48D571E: __vfprintf_internal (vfprintf-internal.c:1687)
==16513==    by 0x48BFD6E: printf (printf.c:33)
==16513==    by 0x484B236: test15() (testLib.cpp:52)
==16513==    by 0x109155: main (test15.cpp:7)
==16513== 
==16513== 
==16513== HEAP SUMMARY:
==16513==     in use at exit: 0 bytes in 0 blocks
==16513==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==16513== 
==16513== All heap blocks were freed -- no leaks are possible
==16513== 
==16513== Use --track-origins=yes to see where uninitialised values come from
==16513== For lists of detected and suppressed errors, rerun with: -s
==16513== ERROR SUMMARY: 5 errors from 5 contexts (suppressed: 0 from 0)

```

**`-g`オプションなし**でビルドした場合は、以下のようにライブラリ名まででる。

```
==17988== Memcheck, a memory error detector
==17988== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==17988== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==17988== Command: ./bin/test15
==17988== Parent PID: 17944
==17988== 
==17988== Conditional jump or move depends on uninitialised value(s)
==17988==    at 0x48D5988: __vfprintf_internal (vfprintf-internal.c:1687)
==17988==    by 0x48BFD6E: printf (printf.c:33)
==17988==    by 0x484B236: test15() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17988==    by 0x109155: main (in /home/kogakz/cpplib/bin/test15)
==17988== 
==17988== Use of uninitialised value of size 8
==17988==    at 0x48B96CB: _itoa_word (_itoa.c:179)
==17988==    by 0x48D55A4: __vfprintf_internal (vfprintf-internal.c:1687)
==17988==    by 0x48BFD6E: printf (printf.c:33)
==17988==    by 0x484B236: test15() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17988==    by 0x109155: main (in /home/kogakz/cpplib/bin/test15)
==17988== 
==17988== Conditional jump or move depends on uninitialised value(s)
==17988==    at 0x48B96DD: _itoa_word (_itoa.c:179)
==17988==    by 0x48D55A4: __vfprintf_internal (vfprintf-internal.c:1687)
==17988==    by 0x48BFD6E: printf (printf.c:33)
==17988==    by 0x484B236: test15() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17988==    by 0x109155: main (in /home/kogakz/cpplib/bin/test15)
==17988== 
==17988== Conditional jump or move depends on uninitialised value(s)
==17988==    at 0x48D6258: __vfprintf_internal (vfprintf-internal.c:1687)
==17988==    by 0x48BFD6E: printf (printf.c:33)
==17988==    by 0x484B236: test15() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17988==    by 0x109155: main (in /home/kogakz/cpplib/bin/test15)
==17988== 
==17988== Conditional jump or move depends on uninitialised value(s)
==17988==    at 0x48D571E: __vfprintf_internal (vfprintf-internal.c:1687)
==17988==    by 0x48BFD6E: printf (printf.c:33)
==17988==    by 0x484B236: test15() (in /home/kogakz/cpplib/lib/libtestLib.so)
==17988==    by 0x109155: main (in /home/kogakz/cpplib/bin/test15)
==17988== 
==17988== 
==17988== HEAP SUMMARY:
==17988==     in use at exit: 0 bytes in 0 blocks
==17988==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==17988== 
==17988== All heap blocks were freed -- no leaks are possible
==17988== 
==17988== Use --track-origins=yes to see where uninitialised values come from
==17988== For lists of detected and suppressed errors, rerun with: -s
==17988== ERROR SUMMARY: 5 errors from 5 contexts (suppressed: 0 from 0)
```



その他機能
- コールトレース
- ヒーププロファイラ



## memleax
指定した時間よりも長くメモリブロックが確保されていた場合にメモリリークであると判断する

コールスタックの ID と共に、発生箇所の詳細が表示されます。コンパイル時にデバッグオプションが有効だったプロセスは、メモリを確保した箇所のソースファイル名と行番号も出てくるようです。

Valgrind のようなツールのように再コンパイルやプロセスの再起動なしに使うことができる

ただし、解放されるまでの時間で判断するということので、キャッシュ等でメモリを使っていると、誤検知が発生してしまうので、今回は使わない。