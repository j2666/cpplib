#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/kogakz/cpplib/lib

valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./log/test01.log ./bin/test01
valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./log/test02.log ./bin/test02
valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./log/test03.log ./bin/test03
valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./log/test04.log ./bin/test04
valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./log/test05.log ./bin/test05
valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./log/test11.log ./bin/test11
valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./log/test12.log ./bin/test12
valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./log/test13.log ./bin/test13
valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./log/test14.log ./bin/test14
valgrind --leak-check=full --show-reachable=yes --error-limit=no --log-file=./log/test15.log ./bin/test15
