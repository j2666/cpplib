#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/kogakz/cpplib/lib

cd src
dirs=`find . -mindepth 1 -maxdepth 1 -type d`

for dir in $dirs;
do
    echo $dir
    cd $dir
    make clean
    cd ..
done
