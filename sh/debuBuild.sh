#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/kogakz/cpplib/lib

export debugOption="-g"

cd src
dirs=`find . -mindepth 1 -maxdepth 1 -type d`

for dir in $dirs;
do
    echo $dir
    cd $dir
    make 
    cd ..
done
