#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif
    bool func01(char *arg01);
    void func02(int arg01);
#ifdef __cplusplus
}
#endif