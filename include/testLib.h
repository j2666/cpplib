
extern "C"
{
    // 領域外アクセステスト用
    void test11(void);
    // 2重開放
    void test12(void);
    // 2重開放
    void test13(void);
    // freeした領域へのアクセス
    void test14(void);
    //初期化していない領域の参照
    void test15(void);
}